#!/bin/bash

export JXP_HOME=`dirname $0`/..
export CLASSPATH="$CLASSPATH:$JXP_HOME/jxp-1.0.0-beta2.jar:$JXP_HOME/jars/log4j-1.2.8.jar:$JXP_HOME/jars/commons-java-1.2.0.jar:$JXP_HOME/jars/commons-invoke-1.0.0.jar"
java org.onemind.jxp.JxpProcessor  $1 $2
