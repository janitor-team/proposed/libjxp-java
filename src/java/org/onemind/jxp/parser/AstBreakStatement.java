/* Generated By:JJTree: Do not edit this line. AstBreakStatement.java */

package org.onemind.jxp.parser;


public class AstBreakStatement extends SimpleNode {
  public AstBreakStatement(int id) {
    super(id);
  }

  public AstBreakStatement(JxpParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JxpParserVisitor visitor, Object data) throws Exception {
    return visitor.visit(this, data);
  }
}
