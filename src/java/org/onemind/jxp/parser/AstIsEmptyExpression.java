/* Generated By:JJTree: Do not edit this line. AstIsEmptyExpression.java */

package org.onemind.jxp.parser;

public class AstIsEmptyExpression extends SimpleNode {
  public AstIsEmptyExpression(int id) {
    super(id);
  }

  public AstIsEmptyExpression(JxpParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JxpParserVisitor visitor, Object data) throws Exception {
    return visitor.visit(this, data);
  }
}
