/* Generated By:JJTree: Do not edit this line. AstExitStatement.java */

package org.onemind.jxp.parser;

public class AstExitStatement extends SimpleNode {
  public AstExitStatement(int id) {
    super(id);
  }

  public AstExitStatement(JxpParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JxpParserVisitor visitor, Object data) throws Exception {
    return visitor.visit(this, data);
  }
}
