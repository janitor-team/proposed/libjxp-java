/* Generated By:JJTree: Do not edit this line. AstBitwiseXOrAssignExpression.java */

package org.onemind.jxp.parser;


public class AstBitwiseXOrAssignExpression extends SimpleNode {
  public AstBitwiseXOrAssignExpression(int id) {
    super(id);
  }

  public AstBitwiseXOrAssignExpression(JxpParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JxpParserVisitor visitor, Object data) throws Exception {
    return visitor.visit(this, data);
  }
}
