/* Generated By:JJTree: Do not edit this line. AstExclusiveOrExpression.java */

package org.onemind.jxp.parser;

public class AstExclusiveOrExpression extends SimpleNode
{

    public AstExclusiveOrExpression(int id)
    {
        super(id);
    }

    public AstExclusiveOrExpression(JxpParser p, int id)
    {
        super(p, id);
    }

    /** Accept the visitor. * */
    public Object jjtAccept(JxpParserVisitor visitor, Object data)
            throws Exception
    {
        return visitor.visit(this, data);
    }
}