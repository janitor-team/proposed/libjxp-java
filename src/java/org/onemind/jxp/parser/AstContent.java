/* Generated By:JJTree: Do not edit this line. AstContent.java */

package org.onemind.jxp.parser;


public class AstContent extends SimpleNode {
  public AstContent(int id) {
    super(id);
  }

  public AstContent(JxpParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JxpParserVisitor visitor, Object data) throws Exception {
    return visitor.visit(this, data);
  }
}
