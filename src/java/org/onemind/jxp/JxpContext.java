/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

import java.io.Writer;
import java.util.*;
/**
 * A jxp context encapsulate the pagesource and default environment.
 * It creates the JxpProcessingContext to the JxpProcessor.
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 */
public class JxpContext
{

    /** the page source **/
    private final JxpPageSource _source;

    /** the default environment **/
    private final Map _defaultEnv;

    /**
     * Constructor
     * @param source the page source
     */
    public JxpContext(JxpPageSource source)
    {
        this(source, Collections.EMPTY_MAP);
    }

    /**
     * Constructor
     * @param source the source
     * @param env the environment
     */
    public JxpContext(JxpPageSource source, Map defaultEnv)
    {
        _source = source;
        _defaultEnv = defaultEnv;
    }

    /**
     * Get the page source
     * @return the page source
     */
    public JxpPageSource getPageSource()
    {
        return _source;
    }


    /**
     * Get the environment
     * @return the environment
     */
    public Map getEnvironment()
    {
        return _defaultEnv;
    }    

    /**
     * Create processing context
     * @param page the page
     */
    public JxpProcessingContext createProcessingContext(Map pageEnv, Writer writer) throws Exception
    {        
        HashMap env = new HashMap(_defaultEnv);
        env.putAll(pageEnv);        
        env.put("writer", writer);
        JxpProcessingContext context = new JxpProcessingContext(writer, env);
        return context;
    }
}