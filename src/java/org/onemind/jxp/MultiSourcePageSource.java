/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

import java.io.IOException;
import java.util.*;
import org.onemind.jxp.parser.AstJxpDocument;
/**
 * A PageSource containing multiple page sources
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public class MultiSourcePageSource extends JxpPageSource
{

    /** the sources **/
    private List _sources = new ArrayList();

    /**
     * Constructor
     */
    public MultiSourcePageSource()
    {
    }

    /**
     * Add a new page source
     * @param source the page source
     */
    public final void addPageSource(JxpPageSource source)
    {
        _sources.add(source);
    }

    /**
     * Remove the page source
     * @param source the source
     */
    public final void removePageSource(JxpPageSource source)
    {
        _sources.remove(source);
    }

    /**
     * Get the page sources
     * @return the sources
     */
    public final List getPageSources()
    {
        return Collections.unmodifiableList(_sources);
    }

    /**
     * {@inheritDoc}
     */
    public final JxpPage getJxpPage(String id) throws JxpPageNotFoundException
    {
        for (int i = 0; i < _sources.size(); i++)
        {
            JxpPageSource source = (JxpPageSource) _sources.get(i);
            if (source.hasJxpPage(id))
            {
                return source.getJxpPage(id);
            }
        }
        throw new JxpPageNotFoundException("Page " + id + " not found");
    }

    /**
     * {@inheritDoc}
     */
    public final StringBuffer getErrorSource(JxpPage page, int line, int col) throws IOException
    {
        return page.getSource().getErrorSource(page, line, col);
    }

    /**
     * {@inheritDoc}
     */
    public final AstJxpDocument getJxpDocument(JxpPage page) throws JxpPageSourceException
    {
        return page.getSource().getJxpDocument(page);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasJxpPage(String id)
    {
        for (int i = 0; i < _sources.size(); i++)
        {
            JxpPageSource source = (JxpPageSource) _sources.get(i);
            if (source.hasJxpPage(id))
            {
                return true;
            }
        }
        return false;
    }
}