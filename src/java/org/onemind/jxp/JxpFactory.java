/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

import java.io.*;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.onemind.commons.java.xml.digest.SaxDigesterHandler;
import org.onemind.jxp.config.JxpConfigDigester;
import org.xml.sax.SAXException;
/**
 * The JxpPageSource factory
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public final class JxpFactory
{

    /** the parser * */
    private static SAXParser _parser;

    /**
     * Constructor
     */
    private JxpFactory()
    {
    }

    /**
     * set up
     * @return the parser
     * @throws Exception if there's problem
     */
    private static SAXParser getParser() throws Exception
    {
        if (_parser == null)
        {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            _parser = factory.newSAXParser();
        }
        return _parser;
    }

    /**
     * Get the page source from the config
     * @param configFile the config
     * @return the config
     * @throws IOException if there's IO problem
     * @throws SAXException if there's parsing exception
     * @throws Exception if there's other problem
     */
    public static JxpPageSource getPageSource(String configFile) throws SAXException, IOException, Exception
    {
        //        PageSourceDigesterElement config = new PageSourceDigesterElement();
        //        SaxDigesterHandler handler = new SaxDigesterHandler();
        //        handler.addDigester(config);
        //        getParser().parse(new File(configFile), handler);
        //        return config.getPageSource();
        return null;
    }

    /**
     * Get the processor from the config
     * @param configFile the config
     * @return the processor
     * @throws IOException if there's IO problem
     * @throws SAXException if there's parsing problem
     * @throws Exception if there's other problem 
     */
    public static JxpProcessor getProcessor(String configFile) throws SAXException, IOException, Exception
    {
        return getProcessor(new FileInputStream(configFile));
    }

    /**
     * Get the processor from the config
     * @param config the config input stream
     * @return the processor
     * @throws IOException if there's IO problem
     * @throws SAXException if there's parsing problem
     * @throws Exception if there's other problem 
     */
    public static JxpProcessor getProcessor(InputStream config) throws SAXException, IOException, Exception
    {
        SaxDigesterHandler handler = new SaxDigesterHandler();
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
        JxpConfigDigester dig = new JxpConfigDigester();
        handler.addDigester(dig);
        parser.parse(config, handler);
        return dig.getProcessor();
    }
}