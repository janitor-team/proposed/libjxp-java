/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp.config;

import org.onemind.commons.java.xml.digest.*;
import org.onemind.jxp.JxpPageSource;
import org.onemind.jxp.MultiSourcePageSource;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
/**
 * TODO comment
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 */
public class MultiSourceDigester extends AbstractElementCreatorDigester implements ElementListener
{

    /** the multi source page source created **/
    private MultiSourcePageSource _source;

    /**
     * Constructor
     */
    public MultiSourceDigester()
    {
        super("multisource");
    }

    /** 
     * {@inheritDoc}
     */
    public void startDigest(SaxDigesterHandler handler, Attributes attr) throws SAXException
    {
        _source = new MultiSourcePageSource();
        //  setup multi
        MultiSourceDigester dig = new MultiSourceDigester();
        dig.addListener(this);
        handler.addSubDigester(dig);
        FileSourceDigester fdig = new FileSourceDigester();
        fdig.addListener(this);
        handler.addSubDigester(fdig);
        StreamSourceDigester sdig = new StreamSourceDigester();
        sdig.addListener(this);
        handler.addSubDigester(sdig);
        setCreatedElement(_source);
    }

    /** 
     * {@inheritDoc}
     */
    public void objectCreated(Object obj)
    {
        _source.addPageSource((JxpPageSource) obj);
    }
}