/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp.ant;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.onemind.jxp.*;
public class JxpAntTask extends Task
{

    private StringBuffer _script;

    private String _scriptPath;

    private MultiSourcePageSource _rootSource;

    private ByteArrayPageSource _bufferedPageSource;

    public JxpAntTask()
    {
        //set up the page source
        setTaskName("jxp");
        _rootSource = new MultiSourcePageSource();
        _bufferedPageSource = new ByteArrayPageSource();
        _rootSource.addPageSource(new ResourceStreamPageSource("/org/onemind/jxp/ant"));        
        _rootSource.addPageSource(_bufferedPageSource);
        _script = new StringBuffer();
    }

    public void setScriptPath(String scriptPath)
    {
        _scriptPath = scriptPath;
    }

    public void addText(String text)
    {
        _script.append(text);
    }

    public void execute() throws BuildException
    {
        if (_script == null)
        {
            throw new IllegalArgumentException("No script has been written in the task context");
        }
        FilePageSource ps = null;
        if (_scriptPath != null)
        {
            ps = new FilePageSource(_scriptPath);
        } else
        {
            ps = new FilePageSource("."); //current directory
        }
        _rootSource.addPageSource(ps);
        Map env = new HashMap();
        env.putAll(project.getProperties());
        env.put("project", super.getProject());
        env.put("location", super.getLocation());
        env.put("description", super.getDescription());
        env.put("target", super.getOwningTarget());
        env.put("taskname", super.getTaskName());
        JxpProcessor processor = new JxpProcessor(new JxpContext(_rootSource, env));
        _script.insert(0, "<%");
        _script.append("%>");
        _bufferedPageSource.putPageBuffer(getTaskName(), _script.toString().getBytes());
        Writer w = new PrintWriter(new OutputStreamWriter(System.out));                
        try
        {
            processor.process(getTaskName(), w);
            w.flush();
        } catch (Exception e)
        {
            e.printStackTrace();
            throw new BuildException(e);
        } finally
        {
            _rootSource.removePageSource(ps);
        }
    }
}