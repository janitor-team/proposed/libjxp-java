/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

import org.onemind.jxp.parser.AstJxpDocument;
/**
 * Represent an JXPPage. It has a name and hold reference to the AST structure
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public abstract class JxpPage
{

    /** the source * */
    private JxpPageSource _source;

    /** the name * */
    private String _name;

    /** the document * */
    private AstJxpDocument _doc;

    /** the error message * */
    private JxpPageParseException _pageParseException;

    /** the encoding **/
    private String _encoding;

    /**
     * Constructor
     * @param source the source
     * @param name the name
     */
    public JxpPage(JxpPageSource source, String name, String encoding)
    {
        _source = source;
        _name = name;
        _encoding = encoding;
    }

    /**
     * Get the name
     * @return the name
     */
    public final String getName()
    {
        return _name;
    }

    /**
     * Get the JxpDocument tree structure
     * @return the document tree structure
     * @throws JxpPageSourceException if there's problem creating the document tree
     */
    public final AstJxpDocument getJxpDocument() throws JxpPageSourceException
    {
        return _source.getJxpDocument(this);
    }

    /**
     * Get the source
     * @return the source
     */
    public final JxpPageSource getSource()
    {
        return _source;
    }

    /**
     * Return the doc
     * @return the doc.
     */
    public final AstJxpDocument getDocument()
    {
        return _doc;
    }

    /**
     * Set the doc
     * @param doc The doc to set.
     */
    public final void setDocument(AstJxpDocument doc)
    {
        _doc = doc;
    }

    /**
     * @param s
     * @return
     */
    public abstract Object declareStaticVariable(String s, Object value);

    /**
     * @param s
     * @return
     */
    public abstract boolean hasStaticVariable(String s);

    /**
     * @param s
     * @return
     */
    public abstract Object getStaticVariable(String s);

    /**
     * @param name
     * @param value
     */
    public abstract Object assignStaticVariable(String name, Object value);


    /**
     * Return the hasParseError
     * @return the hasParseError.
     */
    public final boolean hasParseError()
    {
        return _pageParseException != null;
    }

    /**
     * Set the pageParseException
     * @param pageParseException The pageParseException to set.
     */
    public final void setParseException(JxpPageParseException pageParseException)
    {
        _pageParseException = pageParseException;
    }

    /**
     * Return the pageParseException
     * @return the pageParseException.
     */
    public final JxpPageParseException getParseException()
    {
        return _pageParseException;
    }
    
    /**
     * Return the encoding
     * @return the encoding.
     */
    public final String getEncoding()
    {
        return _encoding;
    }

    
    /**
     * Set the encoding
     * @param encoding The encoding to set.
     */
    public final void setEncoding(String encoding)
    {
        _encoding = encoding;
    }

}