libjxp-java (1.6.1-7) UNRELEASED; urgency=medium

  * Remove myself from Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Fri, 20 Aug 2021 03:33:01 +0200

libjxp-java (1.6.1-6) unstable; urgency=medium

  * Team upload.
  * Fixed the test error on headless systems
  * Transition to the Servlet API 3.1
  * Standards-Version updated to 3.9.7 (no changes)
  * Removed the javadoc from the binary package

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 15 Feb 2016 22:25:02 +0100

libjxp-java (1.6.1-5) unstable; urgency=medium

  * Team upload.
  * Fixed a test failure with Java 8
  * Fixed an IOException (Too many open files) during the build
    (debian-libs.patch caused Ant to load all the jars in /usr/share/java)
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 12 May 2014 14:12:53 +0200

libjxp-java (1.6.1-4) unstable; urgency=medium

  * Add build dependency on libservlet3.0-java. Thanks to David Suárez
    for the bug report. (Closes: #733364)
  * Update Vcs-* fields in debian/control.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 29 Dec 2013 03:49:41 +0100

libjxp-java (1.6.1-3) unstable; urgency=low

  * Update Maintainer field for Java team maintenance.
  * Add gregor to Uploaders.
  * Update Vcs-* headers.

 -- tony mancill <tmancill@debian.org>  Tue, 29 May 2012 07:28:26 -0700

libjxp-java (1.6.1-2) unstable; urgency=low

  * debian/control: change build dependency from default-jdk-builddep to
    default-jdk. Closes: #669216
  * Update Vcs-* headers.
  * Use source format 3.0 (quilt).
  * Use debhelper 8 and tiny debian/rules.
  * Bump Standards-Version to 3.9.3 (no changes).
  * debian/control: remove duplicate build-dependency and unneeded JRE
    runtime dependency, add ${misc:Depends}.
  * debian/copyright: update to Copyright-Format 1.0.

 -- gregor herrmann <gregoa@debian.org>  Wed, 18 Apr 2012 17:06:56 +0200

libjxp-java (1.6.1-1) unstable; urgency=low

  [ gregor herrmann ]
  * Initial release (Closes: #519122)

  [ tony mancill ]
  * Include clarifying statement in debian/copyright regarding SPL
    licensed files now distributed under the BSD license.
  * debian/rules sets DISPLAY (needed for junit tests)

  [ gregor herrmann ]
  * Set Standards-Version to 3.8.3 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Wed, 19 Aug 2009 17:37:16 +0200
